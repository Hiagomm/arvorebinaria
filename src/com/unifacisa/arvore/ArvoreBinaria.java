package com.unifacisa.arvore;

import java.util.Stack;

public class ArvoreBinaria {

	private No raiz;
	
	
	/**
	 * adiciona um elemento a arvore
	 * @param valor
	 */
	public void add(int valor) {
		add(raiz, valor);
	}

	private void add(No no, int valor) {
		if (this.raiz == null) {
			this.raiz = new No(valor);
		} else if (valor < no.getValor()) {
			if (no.getEsquerda() != null) {
				add(no.getEsquerda(), valor);
			} else {
				no.setEsquerda(new No(valor));
			}
		} else {
			if (no.getDireita() != null) {
				add(no.getDireita(), valor);
			} else {
				no.setDireita(new No(valor));
			}
		}
	}
	
	/**
	 * demonstra os valores da arvore de forma identada
	 */
	public void printIdentado() {
		printIdentadoPrivado(raiz, "");
	}

	private void printIdentadoPrivado(No no, String espaco) {
		if (no != null) {
			System.out.println(espaco + no.getValor());
			espaco += "  ";
			printIdentadoPrivado(no.getEsquerda(), espaco);
			printIdentadoPrivado(no.getDireita(), espaco);
		}
	}
	/**
	 * @return a altura da arvore
	 */
	public int altura() {
		return altura(this.raiz);
	}

	private int altura(No raiz) {

		if (raiz == null) {
			return 0;
		}
		int esquerda = altura(raiz.getEsquerda());
		int direita = altura(raiz.getDireita());

		if (esquerda >= direita) {
			return esquerda + 1;
		} else {
			return direita + 1;
		}

	}
	/**
	 * @return a quantidade de nos que uma arvore possui 
	 */
	public int contaNos() {
		return contaNos(this.raiz);
	}

	private int contaNos(No raiz) {
		if (raiz == null) {
			return 0;
		}
		int esquerda = contaNos(raiz.getEsquerda());
		int direita = contaNos(raiz.getDireita());

		return esquerda + direita + 1;
	}

	/**
	 * @return a quantidade de folhas que a arvore possui
	 */
	public int contaFolhas() {
		return contaFolhas(this.raiz);
	}

	private int contaFolhas(No raiz) {
		if (raiz == null) {
			return 0;
		}
		int esquerda = contaFolhas(raiz.getEsquerda());
		int direita = contaFolhas(raiz.getDireita());

		if (raiz.getEsquerda() == null && raiz.getDireita() == null) {
			return esquerda + direita + 1;
		} else {
			return esquerda + direita;
		}
	}
	
	/**
	 * @return um boolean indicando se a arvore � cheia ou n�o
	 */
	public boolean cheia() {
		return cheia(raiz);
	}

	private boolean cheia(No raiz) {
		if (raiz == null) {
			return true;
		}
		if ((raiz.getDireita() == null && raiz.getEsquerda() != null)
		|| (raiz.getDireita() != null && raiz.getEsquerda() == null)) {
			return false;
		} else {
			boolean esquerda = cheia(raiz.getEsquerda());
			boolean direita = cheia(raiz.getDireita());
			return direita && esquerda;
		}
	}
	
	/**
	 * @return uma string com os elementos da arvore em preOrdem
	 */
	public String preOrdem() {
		if (raiz != null) {
			No raiz = this.raiz;
			StringBuilder builder = new StringBuilder();
			Stack<No> pilha = new Stack<No>();
			pilha.push(raiz);
			while (!pilha.isEmpty()) {
				raiz = pilha.pop();
				if (raiz != null) {
					pilha.push(raiz);
					builder.append(raiz + " ");
					pilha.push(raiz.getEsquerda());
				} else if (!pilha.isEmpty()) {
					raiz = pilha.pop();
					pilha.push(raiz.getDireita());
				}
			}
			return builder.toString().trim();
		}
		return "Arvore vazia !!";
	}
	/**
	 * @return uma string com os elementos da arvore em ordem
	 */
	public String inOrdem() {
		if (raiz != null) {
			No raiz = this.raiz;
			StringBuilder builder = new StringBuilder();
			Stack<No> pilha = new Stack<No>();
			pilha.push(raiz);
			while (!pilha.isEmpty()) {
				raiz = pilha.pop();
				if (raiz != null) {
					pilha.push(raiz);
					pilha.push(raiz.getEsquerda());
				} else if (!pilha.isEmpty()) {
					raiz = pilha.pop();
					pilha.push(raiz.getDireita());
					builder.append(raiz + " ");
				}
			}
			return builder.toString().trim();
		}
		return "Arvore vazia !!";
	}

	/**
	 *  	QUEST�O 7
	 *  	RAIZ DO PRECURSO P�S-ORDEM = G 
	 *  	RAIZ DO PRECURSO PRE-ORDEM = I 
	 *  	RAIZ DO PRECURSO P�S-ORDEM = D
	 */
	
	public static void main(String[] args) {
		ArvoreBinaria arvore = new ArvoreBinaria();
		arvore.add(10);
		arvore.add(8);
		arvore.add(12);
		arvore.add(9);
		arvore.add(7);
		arvore.add(11);
		arvore.add(13);
		arvore.add(14);
		arvore.printIdentado();
		System.out.println(arvore.altura());
		System.out.println(arvore.contaNos());
		System.out.println(arvore.contaFolhas());
		System.out.println(arvore.cheia());
		System.out.println(arvore.preOrdem());
		System.out.println(arvore.inOrdem());
	}

}
