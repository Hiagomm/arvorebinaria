package com.unifacisa.arvore;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class ArvoreBinariaTest {
	
	private ArvoreBinaria arvore ;
	
	@BeforeEach
	void setUp() throws Exception {
		arvore = new ArvoreBinaria();
	}

	@AfterEach
	void tearDown() throws Exception {
		arvore = new ArvoreBinaria();
	}

	
	@Test
	void addTest() {
		arvore.add(10);
		arvore.add(8);
		arvore.add(12);
		arvore.add(9);
		arvore.add(7);
		arvore.add(11);
		arvore.add(13);
		arvore.add(14);
		assertEquals(8, arvore.contaNos());
	}
	
	@Test
	void addTest2() {
		arvore.add(1);
		assertEquals(1, arvore.contaNos());
	}
	
	@Test
	void alturaTest() {
		arvore.add(1);
		assertEquals(1, arvore.altura());
	}

	@Test
	void alturaTest2() {
		arvore.add(2);
		arvore.add(1);
		arvore.add(3);
		assertEquals(2, arvore.altura());
	}
	@Test
	void alturaTest3() {
		arvore.add(2);
		arvore.add(3);
		arvore.add(4);
		assertEquals(3, arvore.altura());
	}
	
	@Test
	void alturaTest4() {
		arvore.add(2);
		arvore.add(4);
		arvore.add(3);
		assertEquals(3, arvore.altura());
	}
	
	@Test
	void alturaTest5() {
		arvore.add(2);
		arvore.add(3);
		arvore.add(4);
		arvore.add(5);
		arvore.add(6);
		arvore.add(1);

		assertEquals(5, arvore.altura());
	}
	@Test
	void alturaTest6() {
		assertEquals(0, arvore.altura());
	}
		
	@Test 
	void contaNosTest() {
		assertEquals(0, arvore.contaNos());
	}
	@Test 
	void contaNosTest2() {
		arvore.add(1);
		arvore.add(2);
		arvore.add(3);
		arvore.add(4);
		arvore.add(5);
		arvore.add(6);
		arvore.add(7);
		arvore.add(8);
		arvore.add(9);
		arvore.add(10);
		assertEquals(10, arvore.contaNos());
	}
	
	@Test
	void contaFolhas() {
		arvore.add(1);
		arvore.add(2);
		arvore.add(3);
		arvore.add(4);
		arvore.add(5);
		arvore.add(6);
		arvore.add(7);
		arvore.add(8);
		arvore.add(9);
		arvore.add(10);
		assertEquals(1,arvore.contaFolhas());
	}
	@Test
	void contaFolhas2() {
		arvore.add(2);
		arvore.add(1);
		arvore.add(3);
		assertEquals(2,arvore.contaFolhas());
	}

	@Test
	void contaFolhas3() {
		arvore.add(4);
		arvore.add(2);
		arvore.add(1);
		arvore.add(3);
		arvore.add(6);
		arvore.add(5);
		arvore.add(6);
		assertEquals(4,arvore.contaFolhas());
	}
	@Test
	void contaFolhas4() {
		arvore.add(4);
		assertEquals(1,arvore.contaFolhas());
	}
	
	@Test
	void cheiaTest() {
		assertTrue(arvore.cheia());	
	}
	@Test
	void cheiaTest2() {
		arvore.add(4);
		arvore.add(2);
		arvore.add(6);
		assertTrue(arvore.cheia());	
	}
	@Test
	void cheiaTest3() {
		arvore.add(4);
		arvore.add(2);
		assertFalse(arvore.cheia());	
	}
	@Test
	void cheiaTest4() {
		arvore.add(4);
		assertTrue(arvore.cheia());	
	}
	@Test
	void cheiaTest5() {
		arvore.add(4);
		arvore.add(2);
		arvore.add(6);
		arvore.add(1);
		arvore.add(3);
		arvore.add(5);
		arvore.add(7);
		assertTrue(arvore.cheia());	
	}
	@Test
	void cheiaTest6() {
		arvore.add(1);
		arvore.add(2);
		arvore.add(3);
		arvore.add(4);
		arvore.add(5);
		arvore.add(6);
		arvore.add(7);
		assertFalse(arvore.cheia());	
	}
	@Test
	void preOrdemTest() {
		arvore.add(10);
		arvore.add(8);
		arvore.add(12);
		arvore.add(11);
		arvore.add(7);
		arvore.add(9);
		arvore.add(13);
		arvore.add(14);
		assertEquals("10 8 7 9 12 11 13 14",arvore.preOrdem());	
	}
	
	@Test
	void preOrdemTestArvoreCheia() {
		arvore.add(10);
		arvore.add(8);
		arvore.add(12);
		arvore.add(11);
		arvore.add(7);
		arvore.add(9);
		arvore.add(13);
		assertEquals("10 8 7 9 12 11 13",arvore.preOrdem());	
	}
	@Test
	void preOrdemTest2() {
		arvore.add(1);
		arvore.add(2);
		arvore.add(3);
		arvore.add(4);
		arvore.add(5);
		assertEquals("1 2 3 4 5",arvore.preOrdem());	
	}
	@Test
	void preOrdemArvoreVazia() {
		assertEquals("Arvore vazia !!",arvore.preOrdem());	
	}
	@Test
	void preOrdemTestZigZag() {
		arvore.add(5);
		arvore.add(1);
		arvore.add(4);
		arvore.add(2);
		arvore.add(3);
		assertEquals("5 1 4 2 3",arvore.preOrdem());	
	}
	@Test
	void inOrdemTest() {
		arvore.add(1);
		arvore.add(2);
		arvore.add(3);
		arvore.add(4);
		arvore.add(5);
		assertEquals("1 2 3 4 5",arvore.inOrdem());	
	}
	@Test
	void inOrdemTestZigZag() {
		arvore.add(5);
		arvore.add(1);
		arvore.add(4);
		arvore.add(2);
		arvore.add(3);
		assertEquals("1 2 3 4 5",arvore.inOrdem());	
	}
	@Test
	void inOrdemArvoreVazia() {
		assertEquals("Arvore vazia !!",arvore.inOrdem());	
	}
	@Test
	void inOrdemTestArvoreCheia() {
		arvore.add(10);
		arvore.add(8);
		arvore.add(12);
		arvore.add(11);
		arvore.add(7);
		arvore.add(9);
		arvore.add(13);
		assertEquals("7 8 9 10 11 12 13",arvore.inOrdem());	
	}
	@Test
	void inOrdemTestArvoreNaoCheia() {
		arvore.add(10);
		arvore.add(8);
		arvore.add(12);
		arvore.add(11);
		arvore.add(7);
		arvore.add(9);
		arvore.add(13);
		arvore.add(14);
		assertEquals("7 8 9 10 11 12 13 14",arvore.inOrdem());	
	}
}
